import { str2ab } from './utils.ts';
import { parseRequest } from './httpsigjs/parser.ts';

import type { ParsedSignature } from './httpsigjs/parser.ts';

/**
 * Callback to fetch the public key.
 * You most likely want to fetch the actor with `parsedSignature.keyId`.
 */
type PubKeyGetter = (parsedSignature: ParsedSignature) => Promise<CryptoKey | null> | CryptoKey | null;

/**
 * Verify the signature of a `Request` object. Returns true/false or throws.
 * Since it parses the signature internally, the second parameter is a callback
 * to fetch the public key with the parsed signature's info.
 */
async function verifyRequest(request: Request, getPubKey: PubKeyGetter): Promise<boolean> {
  const parsedSignature = parseRequest(request);
  const pubKey = await getPubKey(parsedSignature);

  if (!pubKey) {
    return false;
  }

  return crypto.subtle.verify(
    'RSASSA-PKCS1-v1_5',
    pubKey,
    str2ab(atob(parsedSignature.signature)),
    str2ab(parsedSignature.signingString),
  );
}

export { verifyRequest };
export type { PubKeyGetter };
