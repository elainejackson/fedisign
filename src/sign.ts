import { Algorithm, generateDigestHeader, sign } from './http-signing-cavage.ts';
import { str2ab } from './utils.ts';

/**
 * Sign a `Request` with the given public key and ID.
 * It will add a `Signature`, `Host`, and `Digest` header to the request.
 * The request is mutated, and you'll want to `await` it.
 */
async function signRequest(request: Request, key: CryptoKey, keyId: string): Promise<void> {
  const signer = async (data: string) =>
    new Uint8Array(
      await crypto.subtle.sign(
        {
          name: 'RSASSA-PKCS1-v1_5',
          hash: 'SHA-256',
        },
        key,
        str2ab(data as string),
      ),
    );
  signer.alg = 'hs2019' as Algorithm;

  if (!request.headers.has('Date')) {
    request.headers.set('Date', new Date().toUTCString());
  }

  if (!request.headers.has('Host')) {
    const url = new URL(request.url);
    request.headers.set('Host', url.host);
  }

  const components = ['@request-target', 'date', 'host'];
  if (request.method == 'POST') {
    components.push('digest');

    if (!request.headers.has('Digest')) {
      const digest = await generateDigestHeader(await request.clone().text());
      request.headers.set('Digest', digest);
    }
  }

  await sign(request, {
    components,
    signer,
    keyId,
  });
}

export { signRequest };
